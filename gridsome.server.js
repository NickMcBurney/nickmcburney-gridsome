module.exports = function (api) {
  api.createPages(({ createPage }) => {
    createPage({
      path: '/website-quote',
      component: './src/templates/QuoteCalculator.vue'
    })
  })
}