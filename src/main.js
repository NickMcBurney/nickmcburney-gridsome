import DefaultLayout from '~/layouts/Default.vue'

require('typeface-lato')

import "@/assets/core.scss";

import marked from 'marked'

import VueGtag from "vue-gtag";


export default function (Vue, { router }) {

  Vue.use(VueGtag, {
    config: { id: "UA-11113644-1" },
    bootstrap: false
  }, router);

  Vue.component('Layout', DefaultLayout)
  Vue.filter('markdown', (string) => marked(string))
}

