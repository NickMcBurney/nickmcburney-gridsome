/** The IntersectionObserver API is instantiated */
const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            const url = entry.target.href;
            //log(url)
            prefetcher(url)
        }
    });
});

/** prefetches a resource */
function prefetcher(url) {
    url = new URL(url, location.href)
    if(navigator && navigator.connection) {
        if(navigator.connection.effectiveType.includes('2g') || navigator.connection.saveData) return
    } else if (sessionStorage.getItem(url) !== null) return
    
    return (supportedPrefetchStrategy)(url).then(() => {
        sessionStorage.setItem(url, 'prefetched')
        console.log(`${url} prefetched`)
    }, () => {
        console.log(`${url} not prefetched`)
    });
};

/** prefetches a resource by using link[rel="prefetch"]
* It creates a link element and appends attributes: rel="prefetch" and href with the url param as the value.
*/
function linkPrefetchStrategy(url) {
    return new Promise((resolve, reject) => {
        const link = document.createElement(`link`);
        link.rel = `prefetch`;
        link.href = url;
        link.onload = resolve;
        link.onerror = reject;
        document.head.appendChild(link);
    });
};
/** prefetches a resource using XHR */
function xhrPrefetchStrategy(url) {
    return new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();
        req.open(`GET`, url, req.withCredentials=true);
        req.onload = () => {
        (req.status === 200) ? resolve() : reject();
        };
        req.send();
    });
}
/** checks if your machine supports prefetching using link[rel="prefetch"] */
function support(feature) {
    const link = document.createElement('link');
    return (link.relList || {}).supports && link.relList.supports(feature);
}
/** Holds the prefetching strategy */        
const supportedPrefetchStrategy = support('prefetch') ? linkPrefetchStrategy : xhrPrefetchStrategy;

const initPrefetch = function () {
    requestIdleCallback(() => {
        Array.from(document.querySelectorAll('a'), link => {
            observer.observe(link);
        });
    }, {
        timeout: 23
    });
}

export default initPrefetch