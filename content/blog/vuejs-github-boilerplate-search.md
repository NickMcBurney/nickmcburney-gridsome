---
title: "My Vue.js GitHub boilerplate search engine"
date: "2017-08-15"
coverImage: "boilerplater-github.png"
excerpt: "I decided last weekend to build a Vue.js powered [boilerplate search engine](https://boilerplater.netlify.app/#/) which could search GitHub for boilerplate repositories using either a search input or common boilerplate categories."
---

I decided last weekend to build a Vue.js powered [boilerplate search engine](https://boilerplater.netlify.app/#/) which could search GitHub for boilerplate repositories using either a search input or common boilerplate categories.

I've played around with Vue before but wanted to improve my skills. Whem im learning new frameworks and wanting to get stuck in fast I often use a boilerplate to get started, so why not build a Vue powered boilerplate search app and improve my skills at the same time.

To build my app I used the [GitHub API](https://developer.github.com/v3/), it was incredibly simple to get started with, and (as you might expect) it has excellent documentation and an active online community making it easy to overcome any bugs/issues I had.

You can try out my boilerplate search app, which I've aptly named [Boilerplater](http://nickmcburney.co.uk/boilerplater/#/) or view the [source code on BitBucket](https://bitbucket.org/NickMcBurney/boilerplater/src).
