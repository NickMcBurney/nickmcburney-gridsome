---
title: "How  I built my first SaaS project: SERPFast.com"
date: "2021-04-28"
coverImage: "serpfast-screenshot.png"
excerpt: "In early April 2021 I release my first Software as a Service (SaaS) project: [SERPFast.com](https://serpfast.com). A tool for comparing Google search result speed metrics. Heres why and how I built my SaaS project."
---

Here is the story of why and how I built SERPFast.com a tool for comparing Google search page results speed metrics.

## 💡 The Idea:
Whilst scrolling through LinkedIn, I saw a post about [this website](https://tools.sandboxweb.io/website-speed-test) and being interested in speed optimisation and the upcoming Google [algorithm update](https://serpfast.com/guides/history-of-google-speed-ranking/) I had a look and signed up for a free account.

I loved the concept but it doesn't (this may change in future) provide a quick view* to compare Core Web Vitals, which is what I was really interested in. **PSI score is OK but if we're interested in SERP then we should [compare Core Web Vitals](https://serpfast.com) since these are what impact the rankings, and not PSI.**

<small>*You can see Core Web Vitals but you must click an option on each result to compare.</small>
 
I'm also not a fan of the site design and result UI, being a frontend developer *with some design experience* I knew I could build something better.

The fact this site existed (I don't know if its successful) also gave me some confidence that there was a market for a tool like this, and if I can make a better version - and give myself the kind of access which I'd have to pay for with the competitor, then why not?

I also knew I could reuse a lot of code from a previous project I had making use of PSI API and Core Web Vitals data to replicate + improve the functionality quite quickly - and **what good developer doesn't love a bit of code reuse.** 😁

## 🌱 The Plan:
I had a rough idea on what functionality I needed, and how I was going to improve on my competitors project.

So I created a Trello board with a few columns: 'Ideas', 'MVP', 'In Progress' and 'Live' and added various cards initially to the MVP column which is how I decided upon the MVP. I used the Ideas column to include additional functionality / ideas for content which I decided were not critical to my MVP*.

*I'm sure im not the first solo SaaS founder who's done this but in the end I let my nerves get the best of me and I delayed any public release to add functionality from the Ideas column - which probably delayed me by 2-3 weeks. These bits may not have been needed but they did make me feel more comfortable about releasing when I did.

## 👨‍💻 The Technology Stack:
So I started building, using a technology stack I knew and loved.

### Frontend:
#### NuxtJS - Vue.js Statically Generated Site
I wanted to work with Vue because its a JS framework I use daily and knew it would be beneficial for the complex API handling and functionality I would need to build. 

I went for a [Nuxt](https://nuxtjs.org/) statically generated site because I wanted the speed + SEO benefit which would be lost with a SPA and the removal of a backend/complex hosting which would be required for an SSR app.

I'm also quite familiar with Nuxt and knew I would be able to quickly prototype my app.

#### TailwindCSS - Utility First CSS Framework
I had not used Tailwind in any previous projects, I've know about it for a few years and its one of those tools regularly talked about within frontend communities.

It sells its self as a lightweight framework to "rapidly build modern websites", being that I wanted to quickly protoype my app I thought it was worth giving it a try.

**My opinion:** I found it did help me very quickly build the UI for my app and I liked the "utility first" idea of the framework. 

It did make me feel a bit horrible when I was attaching so many classes to an element that I had to scroll my IDE to view all the classes and I couldn't help worry about 'HTML bloat', though actually with GZIP [isn't really an issue](https://frontstuff.io/in-defense-of-utility-first-css#it-bloats-the-html).

In this situation I decided to move the CSS to a custom class using [Tailwind's `@apply` directive](https://tailwindcss.com/docs/extracting-components#extracting-component-classes-with-apply): 

```css
.myCustomClass {
    @apply p-4 md:p-6 lg:p-12 flex justify-between text-gray-600 text-lg md:text-xl lg:text-3xl;
}
```

**Note:** this is also not ideal because this causes duplication of CSS which goes against the utility first methodology - but it was a medium ground which I was happy with.

## APIs:
### Scale Serp - Realtime Google Search Results API
I use [Scale Serp](https://www.scaleserp.com/) to query Google search results. The API is easy to use and the response it provides is clean and included everything I needed for my MVP. Scale Serp has a free package which gave me enough room to test out my idea in my local dev environment and it is also one of the cheaper APIs for returning SERP results.

### PageSpeedInsights API - Speed Metrics
I used the [PageSpeed Insights API](https://developers.google.com/speed/docs/insights/rest/v5/pagespeedapi/runpagespeed) to get speed metrics (including Core Web Vitals) and PSI score for each URL.

I already had a small personal project I'd been playing around with which was making use the the PSI API and handling the response to build out Core Web Vitals metric percentiles so I knew I would be able to lift and reuse this code with relative ease. I did refactor my original code as I found a few small issues / improvements I could make.


## Backend:
### Netlify - Hosting, Lambda Functions, Identity and Form Handling
#### Hosting
I've fallen in love with Netlify recently and use it for any personal project im playing around with, and it also hosts this website 😊

Its incredibly easy to get started by simply linking to a git project and giving it the build command (and its designed with JAMstack sites in mind).

#### Lambda Functions
I used [Netlify Functions](https://www.netlify.com/products/functions/) (basically the same as AWS Lambda functions) also allow me to write with JavaScript to handle any logic which I want to remove from the client side code.

#### User Authentication
Something I had not tried using (or even heard of) before was [Netlify Identity](https://docs.netlify.com/visitor-access/identity/), a plug-and-play micro-service for handling site functionality like sign-up, login, password recovery, user metadata, and user roles. Allowing up to 1000 'active' (logged-in visitors within current billing cycle) users for free.

Netlify Identity also has an [npm package](https://github.com/netlify/netlify-identity-widget): `netlify-identity-widget` to provide a UI component for all the basic user authentication logic - which saved a huge amount of time with me not having to build this myself.

####  Form Handling
[Netlify Forms](https://www.netlify.com/products/forms/) make it ease to add forms to your site and link to your Netlify dashboard simple build the HTML form you want and add some Netlify attributes and you can start sending form data to your dashboard.

### Stripe
Stripe is an API for handling payments online and it can be used for creating monthly billing cycles / subscription services - which is what I needed for SERPFast.com.

There is a few options out there for handling paid subscription services like this, I opted for Stripe since its well established and there was plenty of information online to help me get this setup.

A useful tool it comes with (I imagine other services provide this also) is its 'test mode' which allows you to experiment with connecting to the API, purchasing products/subscriptions etc all in test mode - where no actual billing takes place.

### Firebase
I use [Google Firebase](https://firebase.google.com) 'Cloud Firestore' to link-up the data between Netlify Identity and Stripe, this means I can pull a user from their Netlify Identity token and match them to the 'customer' in Stripe as well as store specific customer settings and usage information (like number of searches remaining).

I've played with Cloud Firestore in a few small side projects, and really like how easy it is to work with, and the documentation is pretty easy to understand.


## 🎢 Post Functionality
As with any personal project I work on, I really struggle to get to a point where I'm satisfied with my design and I did quite a few redesigns. 

Once I eventually got the UI right, and I was happy with the MVP functionality I start planning the next steps:

### Pricing
I'm comfortable using my developer skills to build, but pricing a SaaS product is not something I've done before, and I realised it was something which I should have considered earlier on in the project.

I eventually decided on pricing by looking at competitor pricing and working out my costs (assuming some growth in costs and potential high costs as additional functionality/requirements were needed) along with some profit to make my project a viable side income.

### Content production
I wrote a few articles and [conducted some research](https://serpfast.com/guides/are-you-preparing-for-cwv-algorithm/) to help with give a good base for SEO.

I knew this would be important and as well as being good for SEO its also an additional way to draw users to my site through content marketing.

I chose some base subjects related to site speed, and specifically the Core Web Vitals as well as topics which were also relevant to using my product - search as Google [search modifiers](https://serpfast.com/guides/optimize-your-google-searches/).


## 🧪 User Testing
I got this idea from [Simon Høiberg](https://twitter.com/SimonHoiberg) after reading his [How I launched a SaaS Product in 63 days while working full-time](https://www.indiehackers.com/post/how-i-launched-a-saas-product-in-63-days-while-working-full-time-617faf613a) article on IndieHackers.com - this article actually inspired me quite a lot and I'd definitely recommend reading it if you're thinking about creating a SaaS product.

I created a 100% off token using Stripe (which was really easy!) and posted on Reddit and IndieHackers looking for people who might be interested in the project to give it a go and provide some feedback. 

I had a handful of people signed up to the free account and one person who used the coupon for access to the paid plans - with the paid plan coupon I also created a simple survey with SurveyMonkey for some specific feedback. I received some really useful feedback, although I was hoping for more users to provide feedback it was still a very worthwhile task and it helped my refine my product before I publicly announced it.

## 🎉 Public Release
I was now ready to launch / announce the SaaS project I'd built, so I posted on IndieHackers, Reddit and LinkedIn (where I have my largest social following)

**Across all channels I got approximately 5500 'views', 65 interactions (likes etc) and 16 comments (all very positive thankfully) - not a huge amount** 😂

Over the first week I got a little over 100 visits to my site (visits according to Google Analytics - its worth noting my project is GDRP compliant so numbers may actually be slightly higher).

I should have spent more time building a following on other social networks such as Twitter and I could have gained additional buzz around the product - I'm now trying to do this with regular posting and as I add functionality to SERP Fast ill be posting me updates across all channels. The initial buzz and traffic drive from sharing like this is great, but its short lived - the work I've done optimising the site for SEO will pay for its self over time.

Its important for me that as I add new functionality I keep posting to all my channels as this will help drive more interest and traffic to the site.

I plan on also posting to ProductHunt once I have some more functionality built for the project, as I only get one chance with ProductHunt I want to make sure im in the best possible position to do well.

## 💵 Current State:
At time of publishing this article:
- Users: 24 (24 free plan, 0 paid plan)
- Income: £0
- Outgoing: $9/month

## 🏫 What I learnt from my first SaaS
- Your idea doesn't need to be original (though if it is thats also great) - if you can do something better why not?
- TailwindCSS is quite cool - though its not the perfect CSS frame (im not sure the 'perfect' CSS framework exists)
- Building a following ahead of launch can massively help with your initial launch
- Online communities such as [indiehackers.com](https://www.indiehackers.com/) are a great place to get ideas, help and support from fellow indie hackers
