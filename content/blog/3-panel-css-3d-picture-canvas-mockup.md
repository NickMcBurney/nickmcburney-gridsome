---
title: "3 Panel CSS 3D Picture Canvas Mockup"
date: "2020-01-18"
coverImage: "3-panel-CSS-3d-canvas-mock-up.png"
excerpt: "I posted my [CSS only canvas style image/frame](https://codepen.io/NickMcBurney/pen/xxbyXrz?editors=0110) codepen link almost exactly 3 years ago and last month [I was asked](https://nickmcburney.co.uk/blog/css-3d-picture-canvas-mockup/#comments) wether it would be possible to divide one image across more than one panel so thought I’d give it a go this weekend."
useCodepen: true
---

I posted my [CSS only canvas style image/frame](https://codepen.io/NickMcBurney/pen/xxbyXrz?editors=0110) codepen link almost exactly 3 years ago and last month [I was asked](https://nickmcburney.co.uk/blog/css-3d-picture-canvas-mockup/#comments) wether it would be possible to divide one image across more than one panel so thought I'd give it a go this weekend.

Heres what I've come up with, it uses CSS variables to store image size, panel size etc, split the image across three panels - correctly positioning the image and image bleeds.

_It doesn't perfectly answer the question since I was also asked if it would be possible to have the canvas' at different sizes respective to each other - I think it would be if you created new variables for each 'panel' sizing CSS and I might give this a go in the future._

<p class="codepen" data-height="433" data-theme-id="default" data-default-tab="result" data-user="NickMcBurney" data-slug-hash="xxbyXrz" style="height: 433px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="3 Panel CSS 3D Picture Canvas Mockup"><span>See the Pen <a href="https://codepen.io/NickMcBurney/pen/xxbyXrz" rel="nofollow">3 Panel CSS 3D Picture Canvas Mockup </a>by Nick McBurney (<a href="https://codepen.io/NickMcBurney">@NickMcBurney</a>) on <a href="https://codepen.io" rel="nofollow">CodePen</a>.</span></p>
