---
title: "How I included animated SVG icons using shortcodes"
date: "2016-05-10"
coverImage: "using-shortcode-for-inline-svg.jpg"
excerpt: "I use shortcodes to include CSS animated SVG icons on my site without cluttering the WordPress post editor with a bunch of inline SVG code."
---

\[icon-rocket\]Animated SVG rocket

My site has pages which feature animated SVG icons, Originally I included the icons on my site using inline SVG code but this looked horrible when I was pasting the code into the WordPress page editors and made it difficult to edit page content. I needed a way to shorthand my SVGs, I initially thought about adding the SVGs as background images, but SVG animation its compatible with background images. So I decided I would create some WordPress shortcodes to include the inline SVG code without cluttering my page editor.

The function below will get and display the contents of the file using a custom shortcode, you can use this to include inline SVG code, just replace the file URL with the location of your image.
```php
function iconRocket() {
	 $rocket = file\_get\_contents('includes/images/rocket.svg', true); 
	 return $rocket;
};
add\_shortcode( 'icon-rocket', 'iconRocket' );
```
Shortcode: `[icon-rocket]`
