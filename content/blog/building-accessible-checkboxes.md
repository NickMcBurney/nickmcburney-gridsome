---
title: "How I build accessible checkboxes"
date: "2016-07-12"
coverImage: "coding-accessible-checkbox-inputs.png"
excerpt: "Using some common HTML tags and CSS3 pseudo-class selectors we can create accessible checkbox and radio inputs which don't require a mouse to interact with."
useCodepen: true
---

## How I (used to) code styled checkboxes and radio buttons

[Skip to accessible method](#accessible)

Before I understood the importance of accessibility I coded stylised radio buttons using a hidden input and a few span tags, using the inner span to style the button and a bit of jQuery to update the hidden input on clicking.

The issue with this way of doing radio buttons is its **totally in-accessible**, meaning if you don't use a mouse you cant select the options.

##### Heres how not to build accessible buttons: (accessible version is below)

<p data-height="350" data-theme-id="dark" data-slug-hash="GJzbqK" data-default-tab="result" data-user="NickMcBurney" data-embed-version="2" data-pen-title="CCS &amp; jQuery Check Box &amp; Radio Input Custom Styling" data-preview="true" class="codepen">See the Pen <a href="http://codepen.io/NickMcBurney/pen/GJzbqK/">CCS &amp; jQuery Check Box &amp; Radio Input Custom Styling</a> by Nick McBurney (<a href="http://codepen.io/NickMcBurney">@NickMcBurney</a>) on <a href="http://codepen.io">CodePen</a>.</p>

## How I code stylised accessible checkboxes and radio buttons

Using just some basic HTML tags and CSS3 pseudo-class selectors we can create keyboard friendly (accessible) checkbox and radio inputs which can be styled beyond their default styles. [View demo](http://codepen.io/NickMcBurney/pen/RaZrMw "Accessible checkboxes and radio buttons demo")

### HTML Structure

To create accessible stylised inputs I used some basic HTML tags:  
`<fieldset>` to hold it all together, `<legend>` to describe the purpose of the buttons, `<input />` to hold the checked or toggled state, and `<label>` to style our custom button.

**Heres the full HTML structure**
```html
<fieldset class="question">
	<legend>Accessible Radio Buttons:</legend>
	<input type="radio" name="radio" id="yes"><label for="yes">Yes</label> 
	<input type="radio" name="radio" id="no"><label for="no">No</label>
</fieldset>
```
The fieldset tag is used to group related elements in a form or in this case, group related options of the question together. The legend tag defines the question or caption for the fieldset element and this is what is read out by screen readers.

**Its important to link the label to the input using the input's 'id' property and the label's 'for' property to tell the browser that these elements are connected.** Once you add this code you can use the label element to toggle the input's check/unchecked value.

#### Simple example:
```html
<input type="checkbox" id="checkbox"><label for="checkbox">Click me</label>
```

### CSS Stylings

Now we can control the input using the label element we can add styles to make the label look like a button, and add additional styles for when the input is focused and checked. Thanks to CSS3 we can use the `:focused` and `:checked` pseudo-class selectors along with the `+` adjacent selector (which selects only the element which is immediately preceded by the former element) to tell other DOM elements that the input is being focused and checked.
```css
input:focus + label {
	// input is focused highlight sibling label
	box-shadow: 0px 0px 6px 0px rgba(0,142,219,1);
	outline: 0 none;
}

input:checked + label {
	// input is checked style sibling label
	background: #62c8d0;
	color: #fff;
}
```
#### Have look at the full code used below
<p data-height="350" data-theme-id="dark" data-slug-hash="RaZrMw" data-default-tab="result" data-user="NickMcBurney" data-embed-version="2" data-pen-title="Accessible Radio Buttons &amp; Input Fields" data-preview="true" class="codepen">See the Pen <a href="http://codepen.io/NickMcBurney/pen/RaZrMw/">Accessible Radio Buttons &amp; Input Fields</a> by Nick McBurney (<a href="http://codepen.io/NickMcBurney">@NickMcBurney</a>) on <a href="http://codepen.io">CodePen</a>.</p>
<script async src="//production-assets.codepen.io/assets/embed/ei.js"></script>

### Accessible input compatibility

Chrome

Safari

Firefox

Opera

IE

Android

iOS

Any

3.1+

Any

9+

9+

any

any
