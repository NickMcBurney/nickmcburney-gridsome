---
title: "Google Maps JavaScript API - Autocomplete address search"
date: "2018-04-26"
coverImage: "Google-Maps-JavaScript-API.png"
excerpt: "I had read about the Google Maps API years ago but never looked into it, and having been working on an address lookup API recently it reminded me of the service, so I decided to have a look at the Autocomplete API."
useCodepen: true
---

I work with application forms daily and often specifically address lookup and input population functions. I had read about the Google Maps API years ago but never looked into it, and having been working on an address lookup API recently it reminded me of the service, so I decided to have a look at the Autocomplete API.

### Its not suitable for everything

The API uses data from Google Maps, which has lots of [missing data](https://www.google.co.uk/search?q=google+maps+missing+data) (flats, businesses, rural addresses etc) so might not be suitable if you need accurate addresses and don't want to frustrate users who can't find their address.

The javascript API has good [documentation](https://developers.google.com/maps/documentation/javascript/places-autocomplete) and a [full example](https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform). Whilst playing with the demo I noticed that some commercial and landmark type addresses would not populate fully and instead only populated non-specific address details. (Try searching for 'The Trafford Centre' in their [demo](https://google-developers.appspot.com/maps/documentation/javascript/examples/full/places-autocomplete-addressform)).

So I decided to dig in! I copied the demo code into CodePen and had a look at the object returned, which included a 'premise' property containing the information missing from the demo. I added an input for the new property and included the property name and format into the `componentForm` object - which is used to handle the information returned.

<p class="codepen" data-height="520" data-theme-id="0" data-slug-hash="KyzQKp" data-default-tab="result" data-user="NickMcBurney" data-embed-version="2" data-pen-title="Google Maps API - AutoComplete Address - JS">See the Pen <a href="https://codepen.io/NickMcBurney/pen/KyzQKp/">Google Maps API - AutoComplete Address - JS</a> by Nick McBurney (<a href="https://codepen.io/NickMcBurney">@NickMcBurney</a>) on <a href="https://codepen.io">CodePen</a>.</p>
