---
title: "That bug is my fault #1 - IE disabled parent not serialized"
date: "2017-12-03"
coverImage: "that-bug-is-my-fault-1.jpg"
excerpt: "I’m writing about bugs I’ve caused in the hope it’ll stop me repeating them and maybe help someone else making the same mistakes I’ve made."
---

I'm writing about bugs I've caused in the hope it'll stop me repeating them and maybe help someone else making the same mistakes I've made.

## The bug

Applying a `[disabled]` attribute on a `<div>` element\* resulted in the child `<input />` value not being serialised on form submission (affecting IE/Edge only).

**\***The `[disabled]` attribute was used to apply specific styles to a 'pre-populated' form - displaying an 'edit' link and a green tick icon using the `<div>`'s pseudo `:before`.

### The HTML markup:

(excluding the C# used to add the property and populate the input)
```html
<div disabled>
	<label for="fName">First name</label>
	<input type="text" id="fName" name="fName" value="Nick">

	<span class="edit-input">Edit</span>
</div>
```
### The prepopulated style:

![Prepopulated form styles](https://nickmcburney.co.uk/wp-content/uploads/2017/11/form-prepop-screen.png)

### The cause

Inputs with the disabled attribute will not be serialised when using jQuery's `.serialise()` function, IE/Edge handle inputs with a disabled parent as disabled themselves so the child input was not serialised.

Chrome and Firefox don't handle disabled parents in the same way and they **will** serialise the input.

### The fix

I removed the attribute and used a disabled class: `<div class="disabled">` instead.

This is a much better way of doing things and thinking back I'm not sure why I used a disabled attribute over a class. Its not valid HTML and there was no reason to not use a class - other than complicating the CSS/JS.

**You can see the bug in the pen below (use IE).**

<iframe id="cp_embed_VrGEPg" src="https://codepen.io/NickMcBurney/embed/VrGEPg?height=350&amp;theme-id=0&amp;slug-hash=VrGEPg&amp;default-tab=html%2Cresult&amp;user=NickMcBurney&amp;embed-version=2&amp;pen-title=Disabled%20Inputs%20%26%20Serialisation" scrolling="no" frameborder="0" height="350" allowtransparency="true" allowfullscreen="true" name="CodePen Embed" title="Disabled Inputs &amp; Serialisation" class="cp_embed_iframe " style="width: 100%; overflow: hidden;"></iframe>
