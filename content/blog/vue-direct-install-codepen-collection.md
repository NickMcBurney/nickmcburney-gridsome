---
title: "My Vue.js Direct Install CodePen Collection"
date: "2018-06-03"
coverImage: "vue-codepen-collection.jpg"
excerpt: "I built these CodePen examples for a Vue.js training session I ran at work, the training was an introduction to Vue and the pens are very basic examples showing how different parts of the framework function."
useCodepen: true
---

I built these CodePen examples for a Vue.js training session I ran at work, the training was an introduction to Vue and the pens are very basic examples showing how different parts of the framework function.

The examples below use Vue's [direct install](https://vuejs.org/v2/guide/installation.html#Direct-lt-script-gt-Include) which only requires a script tag be included to get started - this means its possible to implement Vue components within non-Vue apps. For full scale Vue apps its best to use the [CLI](https://vuejs.org/v2/guide/installation.html#CLI) which makes it quick to get up and running with hot-reload, JS/CSS compiling, lint-on-save, and production-ready builds.

## Vue.js Interpolation

Vue uses `{{mustache}}` syntax to interpolate JavaScript variables / methods.

The mustache syntax will output the data as plain text which means you cant add HTML this way, if you're trying to add functioning HTML you'll need to use the `v-html:` directive:  
`<span v-html="rawHtml"></span>`

**You can also use regular JavaScript expressions:**
```js
{{ number + 10 }}
{{ isTrue ? 'Yes' : 'No' }}
{{ "description: " + description }}
{{ yourMethod() }}
<span v-html="isTrue ? rawHTML : '<strong>some other html</strong>'"></span>
```
<p data-height="265" data-theme-id="0" data-slug-hash="qVQJyY" data-default-tab="js,result" data-user="NickMcBurney" data-pen-title="Vue.js Interpolation" class="codepen">See the Pen <a href="https://codepen.io/NickMcBurney/pen/qVQJyY/" rel="nofollow">Vue.js Interpolation</a>.</p>

## Vue.js Conditional Show/Hide

Vue uses directives to conditionally render elements based on a true/false expression/boolean.

Using `v-if="true_or_false"` will ensure that event listeners and child components inside the conditional component are destroyed and re-generated when the boolean value changes. If the boolean is false when the parent is initially rendered then no logic from within this component is triggered until the condition is true.

`v-show="true_or_false"` is used to toggle the elements CSS display property, child components and methods etc are rendered in the background.

**Note:** the official documentation states:

> _Generally speaking, v-if has higher toggle costs while v-show has higher initial render costs. So prefer v-show if you need to toggle something very often, and prefer v-if if the condition is unlikely to change at runtime._

You can also use `v-else="true_or_false"` directly beneath a `v-if` element to provide a default alternative, and (as of version 2.1.0) `v-else-if="true_or_false"` for additional conditional options which can also be chained together.

<p data-height="265" data-theme-id="0" data-slug-hash="vWQQYw" data-default-tab="js,result" data-user="NickMcBurney" data-pen-title="Vue.js Conditionals" class="codepen">See the Pen <a href="https://codepen.io/NickMcBurney/pen/vWQQYw/" rel="nofollow">Vue.js Conditionals</a>.</p>

## Vue.js Component Loops

We can use the `v-for` directive to render a list of items based on an array.

The v-for directive uses a syntax similar to `for in` in the form of `x in xyz`, where `xyz` is the source data array and `x` is an alias for the array element being iterated on.

<p data-height="265" data-theme-id="0" data-slug-hash="MOzPqJ" data-default-tab="js,result" data-user="NickMcBurney" data-pen-title="Vue.js Loops" class="codepen">See the Pen <a href="https://codepen.io/NickMcBurney/pen/MOzPqJ/" rel="nofollow">Vue.js Loops</a>.</p>

## Vue.js Event Handling

We use the `v-on` directive to add an event listener to an element or component. The event type is denoted by the argument, e.g `v-on**:click=""**`.

When used on a regular html element, it listens to native DOM events only. When used on a custom element component, it listens to custom events emitted on that child component.

**Tip:** the v-on directive has a shorthand: instead of `v-on:` we can use `@` e.g. `@click=""` and `@keydown=""`

<p data-height="265" data-theme-id="0" data-slug-hash="YERRWo" data-default-tab="js,result" data-user="NickMcBurney" data-pen-title="Vue.js Events" class="codepen">See the Pen <a href="https://codepen.io/NickMcBurney/pen/YERRWo/" rel="nofollow">Vue.js Events</a>.</p>

## Vue.js Model Binding

You can use the `v-model` directive to create two-way data bindings on form input, textarea, and select elements.

`v-model` will automatically pick the correct way to update the element based on the input type.

<p data-height="265" data-theme-id="0" data-slug-hash="RjEbje" data-default-tab="js,result" data-user="NickMcBurney" data-pen-title="Vue.js Model Binding" class="codepen">See the Pen <a href="https://codepen.io/NickMcBurney/pen/RjEbje/">Vue.js Model Binding</a>.</p>

## Vue.js Passing Data to Child (Props)

Vue uses props to pass data from parent components to child components. Props are custom attributes which can be registed on a component. When the parent component passes a value to the prop attribute, it becomes a variable within that component.

<p data-height="265" data-theme-id="0" data-slug-hash="LOXodd" data-default-tab="js,result" data-user="NickMcBurney" data-pen-title="Vue.js Passing Data to Child (Props)" class="codepen">See the Pen <a href="https://codepen.io/NickMcBurney/pen/LOXodd/" rel="nofollow">Vue.js Passing Data to Child (Props)</a>.</p>

## Vue.js Passing Data to Parent ($emit event)

To pass data from child to parent components, we need to use Vue’s `this.$emit(eventName, eventData)` method and `v-on` directive to listen for custom events and handle the data which is sent.

The `$emit` method can be used to pass a string, number, object or array to parent components

<p data-height="265" data-theme-id="0" data-slug-hash="GOeOgX" data-default-tab="js,result" data-user="NickMcBurney" data-pen-title="Vue.js Passing Data to Parent ($emit event)" class="codepen">See the Pen <a href="https://codepen.io/NickMcBurney/pen/GOeOgX/" rel="nofollow">Vue.js Passing Data to Parent ($emit event)</a>.</p>

I'll be adding to the collection and you can see them all here: [https://codepen.io/collection/DkgJLZ/](https://codepen.io/collection/DkgJLZ/)

<script async src="https://static.codepen.io/assets/embed/ei.js"></script>
