---
title: "CSS 3D Picture Canvas Mockup"
date: "2017-01-19"
coverImage: "CSS-3d-canvas-mock-up.jpg"
excerpt: "I built this photo canvas mock-up using just HTML and CSS, the CodePen demo is styled using SCSS and uses variables to define the image url, canvas dimensions, depth and frame type."
useCodepen: true
---

I'm a big fan of [CodePen](https://codepen.io/NickMcBurney) and regularly use it to try ideas, experiment and build prototypes. I built this mock-up because I wanted to see if I could build a photo canvas mock-up using just CSS and HTML.

My mock-up uses CSS3 transform properties and absolute positioning to reflect the image and give it 3d perspective. I used SCSS to make the mock-up easily dynamic and minimise code using mixins. The mock-up has SCSS variables to change canvas image, dimensions and frame type: image wrap, white frame and black frame.

You can play around with my mock-up and update the image url, canvas dimensions, canvas frame depth and type by visiting [my CodePen demo](https://codepen.io/NickMcBurney/pen/ygbvYg/).
