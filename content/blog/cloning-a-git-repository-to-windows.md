---
title: "Installing Git and Cloning a Repository to Windows (3-steps)"
date: "2016-10-07"
coverImage: "Git-and-Windows.png"
excerpt: "This is a quick 3-step guide explaining how to install the Git SCM to Windows and then clone a GitHub repository to your local folder."
---

This is a quick 3-step guide explaining how to install the Git SCM to Windows and then clone a GitHub repository to your local folder.

## Step 1: Download Git

Download Git for windows from: [git-for-windows.github.io](https://git-for-windows.github.io/) and run through the installer.

### Check Git was installed

Open Command Prompt and check git installed by typing: `git –version` You should see a console message saying something like: `git version 2.10.2.windows.1`

## Step 2: Setup working directory

Once you have git installed Using Command Prompt navigate directory to folder you want clone git repository to: `cd YourFolderLocation/YourFolder`

## Step 3: Clone your repository

Clone your repository: `git clone https://github.com/YourUserName/YourRepository.git`

### Get around firewall settings

If you’re working from behind a firewall you may need to update the Git proxy config.

#### Get proxy information

To find out the proxy settings on Windows go to Start Menu > Control Panel > Network and Internet > Internet Options > Connections > LAN Settings > Proxy Server shows Address and Port.

#### Update Git proxy config settings

Update git config to include your proxy settings: `git config –global http.proxy http://YourUsername:YourPassword@Your.Proxy.Address:PortNumber`

Try cloning your repository again.

## Your ready to edit

Navigate to your directory and you should have a new folder with your repository inside.
