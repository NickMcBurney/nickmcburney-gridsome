---
title: "CSS Responsive Column Layout Options"
date: "2018-10-19"
coverImage: "column-layout-methods.jpg"
excerpt: "I was thinking about the different ways it possible to create a responsive column layout and thought I'd do a brief guide of the different methods I can think of, along with simple examples."
useCodepen: true
---

I interviewed a number of junior frontend developers last week and one of the technical questions I asked was how they would create a single/three column responsive layout. This got me thinking as to how I create column layouts and the different ways it's possible.

So I thought I'd write a brief guide to the methods I can think of...(I left out `<table>`).

## CSS float:left columns

Floats have long been used to create multi-column layouts since they received browser support around 1998 (when I was 9!). CSS Floats shift an element to the left or right, allowing content to wrap around the element (which when used for columns will position elements into a row), the float CSS property has its [issues](https://css-tricks.com/all-about-floats/#article-header-id-2) and [requires fixes](https://www.w3schools.com/howto/howto_css_clearfix.asp) to work as expected sometimes. We have much better options nowadays and **floats should not be used for creating column layouts**.

#### Compatability

Floats are a feature of CSS 2.1 and are supported by all browsers.

#### CSS float example
<p data-height="265" data-theme-id="0" data-slug-hash="yRjdPL" data-default-tab="html,result" data-user="NickMcBurney" data-pen-title="CSS Columns - Floats" class="codepen">/p&gt;</p>

## CSS display:inline-block columns

The CSS `display: inline-block;` property was the first alternative I tried to CSS float columns, and its a good option for wide browser support, consistency and ease. Much like using float, column widths are controlled with a percentage value.

If your column elements are not connected without space then you'll notice that the columns won't fit into your row. Theres a few ways you can fix this such as removing any whitespace or using HTML comments such as:
```html
<!-- will break because of the line break adding space -->
<div class="col"></div>
<div class="col"></div>

<!-- space removed -->
<div class="col"></div><div class="col"></div>

<!-- space commented out -->
<div class="col"></div><!--
--><div class="col"></div>
```
The options above will fix the spacing issue and bring your columns into a single row, but it makes for messy hard to read code and is not really suitable.

You can use CSS to remove the white space (or zero it), by setting the parent container's `font-size:0` - this will also zero the whitespace and bring your columns inline. Its important to note, that if you have and text directly within the parent container or within you're columns then you'll need to set the `font-size` on these elements.

#### Compatability

IE8+ Chrome 4+ Firefox 2+ Safari 3.1+ IOS Safari 3.2+

#### CSS display inline-block example

  

**This is the method which I most commonly use for creating layouts, because of its** **[compatibility support](https://caniuse.com/#feat=inline-block)** **and ease.**

## CSS display:flex columns

Flexbox has been around for a few years and has decent support with [modern browsers](https://caniuse.com/#feat=flexbox), its very _flexable_ and has some great benefits such as simple responsive and equal height columns. But it does still have [some issues](https://github.com/philipwalton/flexbugs) - especially in IE as you may expect, and shouldnt be used if you need to support older browsers.

#### Compatability (new syntax only)

IE10/11 with lots of bugs Edge 12+ Chrome 21+ Firefox 28+ Safari 6.1+ IOS Safari 7+

#### CSS display:flex example

(a very basic example, it's worth reading a proper [guide to flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) and having a [play around](https://flexboxfroggy.com/))

I've recently started using flexbox in production, in situations when I know the browser is going to be compatible and I'm looking forward to when I can roll it out fully.

## CSS display:grid columns

The `display:grid;` is the most powerful CSS layout module and can be used to create complex column and row layouts which can be rearranged and reordered to fit different screen sizes. Grid layouts have ok [support with modern browsers](https://caniuse.com/#feat=css-grid), but IE10/11 only support the older grid syntax and there's no support for older versions - some other less popular browsers also do not yet support grid layouts.

#### Compatability (new syntax only)

IE no support Edge 16+ Chrome 57+ Firefox 52+ Safari 10.1+ IOS Safari 10.3+

#### CSS display:grid example

  

(another very basic example, to learn more read this [guide](https://css-tricks.com/snippets/css/complete-guide-grid/) and play the [GridGarden](https://cssgridgarden.com/) game)
