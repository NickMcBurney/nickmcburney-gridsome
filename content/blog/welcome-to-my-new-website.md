---
title: "Welcome to my new site"
date: "2016-03-25"
coverImage: "my-responsive-website-design.jpg"
excerpt: "Welcome to my new website design, my old site used the same design for 5 years and it was time for a refresh! This post covers my new custom WordPress theme, the new (responsive) design, the changes to plugins and a new feature."
---

Ive redesigned and rebuilt my portfolio, this post will (try to) explain what has changed, what has improved and why I made the changes I did.

## I coded a custom WordPress theme for my site

When I was designing my new site I knew I wanted to code a custom theme from the ground-up, using no 3rd-party frameworks and as little unneeded code as possible. My previous site (which I built whilst I was first learning WordPress) used a premium WordPress theme which I heavily modified to fit my design. The theme was really nice and came with loads of cool features but this resulted in lots of bloat on my site which slowed it down.

My portfolio and quote calculator pages include 3rd-party scripts, but the others do not and I avoided CSS frameworks because although they can be useful they often add code which just isn't needed or used. By doing this and writing clean custom JavaScript and CSS I have reduced the number of requests my website makes and improved its speed.

## Design changes and responsive web design

I designed this site to be professional, legible and fully responsive.

One of the big changes from my previous design is the switch from serif based typography (I used Droid Serif) to the more legible Open Sans for body copy and uppercase Oswald for page headings.

I also decided to remove the striped background pattern and go for a solid white background, this gives the site a more minimal look and focuses users on the content of my site. I have kept to a simple colour scheme which is used to highlight titles and calls to action.

My previous site was not responsive and this was a major reason for the redesign (admittedly a little later than it should have been), this new site is designed to look beautiful and function perfectly on any device or desktop computer.

## Plugins and features

This theme has many of the same features as my previous site but instead of using plugins to achieve them and I have written them into the theme myself, I have also added some features, such as my [website quote calculator](http://nickmcburney.co.uk/website-quote/).

As I built the theme for my site I made sure I included everything needed in the theme, so I wouldn't require plugins for simple functionality. My previous website included plugins for just about everything - resulting in a total of 13 active plugins and 5 inactive plugins, which all slowed down by site by bloating the database. Now I have 3 active / 0 inactive plugins.

## Conclusion

I'm really pleased with the new design and the new custom theme I built, it loads extremely fast and shows-off some of my best skills, the portfolio shows of my work and the site is better all round.
