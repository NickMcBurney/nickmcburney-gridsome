---
title: "My Angular Debt Calculator"
date: "2017-11-27"
coverImage: "debt-calculator-screenshot.png"
excerpt: "This is an Angular2 debt calculator which I built as a learning project using the Angular-CLI. The calculator will accept multiple debts (amount, monthly payments and term or APR) and display the total debt, total monthly payments, cost of the credit borrowed and the total time to repay based on current debt / payments."
---

Calculators are common components in my work place and I find they make for good learning projects which help me to cement new skills. So I decided I would build a debt calculator (based on a similar calculator [I built with knockout.js](https://codepen.io/NickMcBurney/pen/QEGmQP)) using Angular2​ as a way to improve my understanding of Angular.

[View demo](https://nickmcburney.github.io/ng-debt-calculator/)

You can view the [source code](https://github.com/NickMcBurney/ng-debt-calculator) and [documentation](https://nickmcburney.github.io/ng-debt-calculator/documentation/overview.html) on GitHub.

![Debt Calculator Nexus 5X Screenshot](https://nickmcburney.co.uk/wp-content/uploads/2017/05/Angular-Debt-Calculator-Nexus-5X-Screenshot.png)

### Calculator requirements:

- User should be able to add different debt types
- User should be able to add, edit and remove debts
- Calculate total debt
- Calculate total monthly payments
- Calculate total cost of credit
- Calculate time to repay credit cards (based on credit, APR, monthly payments)
- Calculate max debt term
- Basic validation (monthly payments cover interest)
- Input formatting on keyup, currency and percentages

### Calculator Layout: CSS grid

I also used the new CSS grid spec (which I hadn't used before) for the responsive layout, I only touched the surface but once I got my head around it I started too see why its been described as _"the most powerful layout system available in CSS"_ - [CSS-tricks.com](https://css-tricks.com/snippets/css/complete-guide-grid/)

`display: grid;` has [pretty poor browser support](https://caniuse.com/#feat=css-grid) and when I checked it in IE 11 it failed pretty spectacularly showing only a third of the calculator so please view the demo in a modern (non IE browser).

### Not production ready 😀
