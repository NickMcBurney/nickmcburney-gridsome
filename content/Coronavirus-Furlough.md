---
title: "Coronavirus 2020: Developers on furlough"
date: "2020-03-25"
coverImage: "developer-work-from-home.jpg"
excerpt: "A list of developer tasks / training ideas which I'd be filling my time with if I found myself on furlough leave."
---

Many businesses around the globe are being impacted by the economic uncertainty surrounding Coronavirus. In the UK, the government has set up the Coronavirus Job Retention Scheme, whereby the government will pay 80% of a person's salary for companies who put staff on furlough leave -- which basically means the employee is still employed but on leave and not allowed to contribute to the business whilst on furlough.

As well as the expectantly impacted jobs (such as waiters, retail staff, hotel staff etc) this is also affecting developers who are now finding themselves with an additional 8 hours of their day to fill whilst stuck in lock-down, so I thought I'd put together a list of developer tasks / training ideas which I'd be thinking about if I found myself on furlough leave.

**Note this list is specific to front-end developers** looking to occupy time with development related time-fillers, there are already many great ideas for non-development things to keep you busy.

## GIVING BACK


The developer community is massive, and often very helpful, providing answers to questions and open source plugin's which make our lives easier. We can contribute to this ecosystem through:

### SUPPORT DEVELOPERS ON STACKOVERFLOW.COM

Browse through the StackOverflow questions (you can filter by issue topic, e.g. CSS, JS, Vue, React etc) answer any questions you think you can help with. Don't worry if questions already have answers, sometimes these can be improved upon or updated with newer tech.

### CONTRIBUTE TO OPEN-SOURCE CODE PROJECTS

Open source projects from small plugins to full frameworks such as Vue.js are the backbone of any developer's workflow, without them we'd be spending much more time trying to work these things out ourselves.

We can support open-source plugins through reviewing issues providing answers where possible and -- if you think you have a fix -- fixing and directly contributing to the projects growth (fork, fix/improve, test, merge).

If digging into the code sounds bit daunting then see if the project documentation needs any improvements, many projects have a partial documentation which you could help complete.

## DEVELOPER TASKS


Mini-tasks and projects to keep skills fresh.

### CODE CHALLENGES:

**UI Challenges**
https://collectui.com/challenges
https://www.dailyui.co/
https://www.frontendmentor.io/ (full 'real world' website challenges with design assets and briefs/requirements)

**Code Challenges**
https://www.codewars.com/?language=javascript

### BUILD A "CHEAT SHEET" WEBSITE

Build a simple website to store 'cheats', these could include things such as:
- JavaScript snippets (e.g. scroll to top, loop through array)
- CSS snippets
- Best practices
- Bugs you've fixed -- issue/solution

**Examples:**
https://htmlcheatsheet.com/js/
https://flaviocopes.com/vue-cheat-sheet/

**Ideas:**
- Try using Tailwind or some other CSS framework which you've not used before\
- Build a statically generated site e.g. Grdisome (Vue.js), Vuepress (Vue.js), Gatsby (React.js)

### RICH AND MORTY API MINI SITE

Using the free Rick and Morty API build a simple site which displays each character's profile picture and basic information.

**API:** https://rickandmortyapi.com/

**Alternative APIs**
- Star Wars: https://swapi.co/
- Star Trek: http://stapi.co/

### COMPARE VUE, REACT AND {{ INSERT JS FRAMEWORK }}

Build a simple component and replicate with either Vue, Angular or React.

Compare differences in syntax, complexity, re-usability, development time, bundle size and load times etc.

### REDESIGN AND REBUILD A PERSONAL WEBSITE AS A STATIC WEBSITE

Use a static site generator, such as Gridsome (Vue.js) or Gatsby (React.js) to build a new lightning fast personal website/blog.

**Ideas:**
- Integrate with a headless CMS (or WordPress?!) to provide content\
- Deploy the project through Netlify build pipeline

### BUILD AN ACCESSIBLE RANGE SLIDER COMPONENT

**Requirements:**
- Slider should be controlled via keyboard (e.g. tabbing, left/right keys to adjust values)
- Slider should announce the min/max values after question text is read out
- Slider should announce value after update

**Accessibility tips:**
- Look at aria attributes for question text, min/max values and current value
- Install a free screen reader (windows 10 has one built in) or Chrome screen reader plugin

### BUILD A COVID-19 STATS DASHBOARD

Find a free API for COVID-19 stats and use this data to build a dashboard.

**APIs:**
https://rapidapi.com/Gramzivi/api/covid-19-data
or one of many others: https://rapidapi.com/collection/coronavirus-covid-19

**Dashboard should/could contain:**
(either worldwide or country specific)
- current infected
- current released
- current deaths
- death to infected rate
- chart showing increasing infected rates
- chart showing death to infected % rates
- anything else you can think of