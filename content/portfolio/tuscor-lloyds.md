---
title: "Tuscor Lloyds"
date: "2014-10-20"
image1: "tuscor-lloyds/tuscor-lloyds-homepage.png"
image2: "tuscor-lloyds/tuscor-lloyds-case-studies.png"
image3: "tuscor-lloyds/tuscor-lloyds-services.png"
categories: ["web design and development"]
---

I built this website in 2014 whilst working full-time for Tuscor Lloyds Ltd. This is a completely hand-build responsive WordPress theme with features including a separate news and project blogs, social media sharing and parallax / video backgrounds. 

The theme is also highly optimised for speed and search engine optimisation. These optimisations helped the website rank in top positions for a number of the companies key service keywords.
