---
title: "Coventry University Students Union"
date: "2010-08-12"
image1: "coventry-university-students-union/CUSU.jpg"
image2: "coventry-university-students-union/CUSU-logo-unveiling.jpg"
categories: ["logo design"]
---

This was a competition brief for the Coventry University Students' Union (CUSU).

I was asked to produce a new logo to encompass all areas of the students union and appeal to a large audience. Importantly the logo had to depict the mission of CUSU graphically, attract new members, be effective in both print and web use, incorporate all of the Unions sub-brands and be diverse enough to attract members to both the entertainment venue and the student services.

In response to this I designed a logo which both used bright colours to differentiate the unions sub-brands and attract my target audience: current students/ prospective students, investors/ possible investors and the staff at the students union.

My logo design made it to the public vote alongside two other logos and at the end of the vote my logo had received almost 60% of the vote and is now in use throughout the CUSU branding.



