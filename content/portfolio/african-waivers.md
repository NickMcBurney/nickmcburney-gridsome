---
title: "African Waivers"
date: "2015-10-25"
image1: "african-waivers/african-waivers-homepage.png"
image2: "african-waivers/african-waivers-page-index.png"
image3: "african-waivers/african-waivers-page.png"
categories: ["web design and development"]
---

African Waivers uses a custom responsive WordPress theme which I hand-coded. The site is designed to be simple to use and fast loading (although the Google Ads don’t help). The theme is optimised for on-site search engine optimisation and includes Schema rich snippet code.

