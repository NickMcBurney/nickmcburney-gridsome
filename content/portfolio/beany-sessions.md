---
title: "Beany Sessions"
date: "2012-12-10"
image1: "beany-sessions/beany-sessions-3d-flyer.jpg"
image2: "beany-sessions/SummerSessions.jpg"
image3: "beany-sessions/BackToBackMarathon.jpg"
categories: ["print design"]
---

This flyer was designed for a Manchester based club night, the event was on New Years Eve and was designed to look 3D when looked through 3D glasses. I organised for the printing of the flyers and also supplied 300 3D glasses which were handed out on the night.

I also did a number of other flyer designs for different events.



