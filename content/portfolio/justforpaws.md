---
title: "Just for Paws"
date: "2012-08-24"
image1: "just-for-paws/just-for-paws-logo.jpg"
image2: "just-for-paws/just-for-paws-website.jpg"
image3: "just-for-paws/just-for-paws-dog-training.jpg"
categories: ["logo design", "web design and development"]
---

This logo was designed for a dog walker based in Manchester. The client also needed a website, business cards and letterheads designing. I designed and developed everything needed for this new business and help them with the printing of their brand assets and stationary.