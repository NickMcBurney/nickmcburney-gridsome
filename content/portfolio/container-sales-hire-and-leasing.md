---
title: "Container Sales, Hire & Leasing"
date: "2011-12-06"
image1: "container-sales-hire-and-leasing/container-sales-hire-and-leasing.jpg"
image2: "container-sales-hire-and-leasing/container-sales-hire-and-leasing-homepage.png"
image3: "container-sales-hire-and-leasing/container-sales-hire-and-leasing-printed-ad.jpg"
categories: ["web design and development", "logo design"]
---

The complete design for this new business was produced for Manchester based shipping agent; Tuscor Lloyds Ltd where I worked as a full time web and graphic designer between October 2010 and January 2015.

<strong>Branding</strong>

Container Sales, Hire &amp; Leasing would have strong printed advertisements and a bold web presence within the container sales industry so I had to design a logo which would stand-out from the competition and work well across all media.

<strong>Website Design</strong>

The site I designed is a modern website which is easy for users to navigate whilst providing the content in an appealing and informative way.

<strong>Print Design</strong>

This printed advert is published in an industry magazine read throughout the UK. The paper stock which is used for the relevant section of the magazine is of a low quality, because of this I kept the design simple and informative using only a small colour pallet.