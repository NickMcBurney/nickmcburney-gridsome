---
title: "Premier Accounts & Consultancy"
date: "2014-07-11"
image1: "premier-accounts/premier-accounts.png"
image2: "premier-accounts/premier-accounts-blog.jpg"
image3: "premier-accounts/premier-accounts-services.jpg"
categories: ["web design and development"]
---

Premier Accounts &amp; Consultancy Ltd were my very first freelance client back in 2011 and I have worked with their team many times since. I worked closely with Premier Accounts management to completely redesign and rebuild the site in 2014.

Their website now uses a customised WordPress theme which includes a blog, contact forms, email signup form and responsive design. We have improved the site over time and in February 2016 I added a hand-coded Javascript quote calculator to provide web users with an online quote and the ability to submit the quote which emails the price and service details to Premier Accounts and the user.