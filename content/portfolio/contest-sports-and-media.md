---
title: "Contest Sports & Media"
date: "2016-02-01"
image1: "contest-sports-and-media/contest-sports-homepage.png"
categories: ["web design and development"]
---

I designed this site for Contest Sports & Media based in Warrington. I built a custom WordPress theme with responsive design, the theme makes it easy for their team to update the site with new blog posts, events and team members.
