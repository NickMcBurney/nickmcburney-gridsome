---
title: "Brapple"
date: "2016-08-22"
image1: "brapple/Brapple-Logo-1.jpg"
image2: "brapple/Brapple-Logo-2.jpg"
image3: "brapple/Brapple-Logo-3.jpg"
categories: ["logo design"]
---

I designed these logos for Brapple, a new start up business operating a food and drink takeaway. I create six inital logo designs for the client, who then choose one design to further refine.

I provided the client with his new logo in a couple of different formats for different use cases.