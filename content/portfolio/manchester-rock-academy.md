---
title: "Manchester Rock Academy"
date: "2014-01-19"
image1: "manchester-rock-academy/manchester-rock-academy-homepage.png"
image2: "manchester-rock-academy/manchester-rock-academy-logo.jpg"
image3: "manchester-rock-academy/manchester-rock-academy-services.jpg"
categories: ["web design and development", "logo design"]
---

I designed a logo and website for Manchester Rock Academy a local music school and tutition startup. I created a custom WordPress theme for the client and designed the site to be responsive. The custom theme included a blog which was easy for the client to update as needed.


