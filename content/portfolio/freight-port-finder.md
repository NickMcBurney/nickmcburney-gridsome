---
title: "Freight Port Finder"
date: "2012-02-13"
image1: "freight-port-finder/freight-port-finder-homepage.png"
image2: "freight-port-finder/freight-port-finder-single-port.jpg"
image3: "freight-port-finder/freight-port-finder-logo.jpg"
categories: ["web design and development", "logo design"]
---

I designed the logo and website for Freight Port Finder as a side project. I came up with the idea whilst working in the shipping industry. I wanted to build a website providing relevant information / facts about shipping ports (such as water depths and crane capacity) around the globe, and display this information in a easy to visualise way.

The homepage includes a full screen map of the world with maps listed in their location, this makes it easy for users to find ports they're looking for or find ports near destinations they are looking to ship to / from.

The site also included a form which could be used for adding specific information to each port, this would first come to me for review and I could then publish the updates to the website.