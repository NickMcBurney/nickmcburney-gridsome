---
title: "Ready Steady Sew"
date: "2012-12-01"
image1: "ready-steady-sew/Ready-Steady-Sew-Logo-Design.jpg"
categories: ["logo design"]
---

I was asked by the owner of Ready Steady Sew; a start-up company teaching clients how to use sewing machines for creative purposes, to design a logo for web and print use.

The brief I was given asked for a retro design, pastel colours and I was specifically asked to use stitched lines within the design to help symbolise sewing and provide a strong brand for Ready Steady Sew.