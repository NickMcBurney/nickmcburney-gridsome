---
title: "Science Communicated"
date: "2017-05-11"
image1: "science-communicated/science-communicated-web-design.png"
image2: "science-communicated/science-communicated-logo-design.png"
image3: "science-communicated/science-communicated-testimonials-design.png"
image4: "science-communicated/science-communicated-teaching.png"
categories: ["web design and development", "logo design"]
---

Science Communicated Ltd had no previous logo and <a href="https://sciencecommunicated.co.uk" target="_blank" rel="noopener noreferrer">sciencecommunicated.co.uk</a> used an outdated non-responsive WordPress theme. I was contacted to design a professional logo and redesign/code the website.

I designed 8 logos which were refined in 3 iterations to the final logo design and once we had the logo finalised I started on the website design.

The web design was refined in Adobe Illustrator and I began coding a new WordPress theme using one of my responsive template skeleton themes.

I ensured that the new theme/design would work with existing plugins providing core functionality and content (such as event management and testimonial plugins), meaning the client would not need to learn to use any new plugins.

The site is optimised for mobile and tablet devices and is designed to encourage people to signup for the newsletter as well as provide course information and trust in the brand/client.

I also provided Twitter profile, Facebook profile, email banner and business card designs