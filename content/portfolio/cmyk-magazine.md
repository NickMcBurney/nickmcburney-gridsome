---
title: "CMYK magazine"
date: "2010-06-28"
image1: "cmyk-magazine/cmyk-cover.png"
image2: "cmyk-magazine/spread1full.png"
image3: "cmyk-magazine/spread2full.png"
categories: ["print design"]
---

CMYK is a magazine concept I created as a University project whilst at Coventry University. It is a Street & Haute Couture Fashion Magazine based around CMYK which are the colours used in four colour printing (Cyan, Magenta, Yellow and Black). I designed the front cover and 4 spreads for the magazine. (I was forced to use Comic Sans for the font as part of this project.)




