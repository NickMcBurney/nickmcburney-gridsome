---
title: "DV8 Designs"
date: "2015-05-14"
image1: "dv8-designs/dv8-designs.png"
image2: "dv8-designs/dv8-designs-projects.png"
image3: "dv8-designs/dv8-designs-team.png"
categories: ["web design and development"]
---


This website was designed and developed for DV8 Designs, an architecture and interior design company based in Warrington. I met with them at their offices, discussed their requirements and came up with 4 unique designs.

These designs were then refined to a final design, which I then coded as a custom responsive WordPress theme to fit into their existing WordPress setup.



