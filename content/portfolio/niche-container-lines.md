---
title: "Niche Container Lines"
date: "2016-01-25"
image1: "niche-container-lines/niche-container-lines-flyer.jpg"
categories: ["print design"]
---

This flyer was designed for an international client providing logistics services between Europe and South America. I also created a variation of the design in Spanish.