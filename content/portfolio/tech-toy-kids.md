---
title: "Tech Toy Kids"
date: "2014-11-25"
image1: "tech-toy-kids/tech-toy-kids-homepage.png"
image2: "tech-toy-kids/tech-toy-kids-blog.png"
image3: "tech-toy-kids/tech-toy-kids-product.png"
categories: ["web design and development"]
---

TechToyKids.com is an eccomerce website I setup using WordPress. I installed a basic WooCommerce theme and customised the theme code to my design. I also improved the coding and optimised the layout of the theme to improve the user experience and on-page search engine optimisation.

