---
title: "Privacy Policy"
date: "2020-04-18"
---

**Definitions:** 'me', 'my', 'I': Nick McBurney - owner of this website.

Your privacy is important to me. It is my policy to respect your privacy regarding any information I may collect from you across this website, [https://www.nickmcburney.co.uk](https://www.nickmcburney.co.uk).

I only ask for personal information which I need to provide a service and correspond with you. I collect it by fair and lawful means (contact/quote forms), with your knowledge and consent.

I only retain collected information for as long as necessary to provide you with your requested service. What data I store, I protect within commercially acceptable means to prevent loss and theft, as well as unauthorised access, disclosure, copying, use or modification.

I don’t share any personally identifying information publicly or with third-parties, except when required to by law.

My website may link to external sites that are not operated by me. Please be aware that I have no control over the content and practices of these sites, and cannot accept responsibility or liability for their respective privacy policies.

You are free to refuse our request for your personal information, with the understanding that I may be unable to provide you with some of your desired services.

Your continued use of my website will be regarded as acceptance of my practices around privacy and personal information. If you have any questions about how I handle user data and personal information, feel free to [contact me](/contact-me). 

### Cookies

#### What Are Cookies
As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience.

For more general information on cookies, please read ["What Are Cookies"](https://www.cookieconsent.com/what-are-cookies/).

#### How I use cookies
I use cookies to provide analytical information.

I use cookies required for Google Analytics which is one of the most widespread and trusted analytics solution on the web for helping me to understand how you use the site and ways that I can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so I can continue to produce engaging content.

| Name           | Purpose                    | More information   |
|----------------|----------------------------|--------------------|
| \_ga            | Used to distinguish users. | [link](https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage#analyticsjs)|
| \_gid           | Used to distinguish users. | [link](https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage#analyticsjs)|
| \_gat_gtag_UA\_* | Used to distinguish users. | [link](https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage#analyticsjs)|


#### Disabling Cookies

You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help for how to do this).

You can also block the cookies used on this website by clicking 'Block analytics' on the consent popup window which is shown when arriving on this website - this would required on every individual visit to this website.