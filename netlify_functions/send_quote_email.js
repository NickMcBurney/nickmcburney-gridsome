
const { SENDGRID_API_KEY } = process.env;

exports.handler = function (event, context) {
  //var querystring = require('querystring');

  console.log('received quote form body', event.body)

  const body = JSON.parse(event.body);
  const sender_name = body.name
  const sender_email = body.email
  const email_body = body.body
  const design_required = body.design_required
  const template_count = body.template_count
  const page_count = body.page_count
  const blog_required = body.blog_required
  const quote_price = body.quote_price

  console.log('setup mailjet email')

  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(SENDGRID_API_KEY);
  const msg = {
    to: 'nick@nickmcburney.co.uk',
    from: 'nick@nickmcburney.co.uk',
    templateId: 'd-da095ff7639f4e9ea4416156a1a41897',
    dynamic_template_data: {
      name: sender_name,
      email: sender_email,
      message: email_body,
      design_required: design_required ? 'Design Required' : 'Design NOT Required',
      template_count: `Template Count: ${template_count}`,
      page_count: `Page Count: ${page_count}`,
      blog_required: blog_required ? 'Blog Required' : 'Blog NOT Required',
      quote_price: `Quote Price: ${quote_price}`
    },
  };
  sgMail.send(msg);

  /*
  const request = mailjet.post('send', { version: 'v3.1' }).request({
    Messages: [
      {
        From: {
          Email: 'nick@nickmcburney.co.uk',
          Name: 'Nicks Website',
        },
        ReplyTo: {
          Email: sender_name,
          Name: sender_email
        },
        To: [
          {
            Email: 'nick@nickmcburney.co.uk',
            Name: 'Nick McBurney',
          },
        ],
        TemplateID: 1355251,
        TemplateLanguage: true,
        Subject: 'Message from NickMcBurney.co.uk',
        Variables: {
          name: sender_name,
          email: sender_email,
          body: "Testing some email body content",
        }
      },
    ],
  })

  request.then(() => ({
    statusCode: 200,
    body: result.body
  }))
  .catch(err => ({
    statusCode: 200,
    body: "Error:" + err.statusCode
  }))*/
}