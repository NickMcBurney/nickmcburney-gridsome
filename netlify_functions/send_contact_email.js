
// const { MailJetAPI, MailJetSecret } = process.env;
const { SENDGRID_API_KEY } = process.env;
exports.handler = function (event, context) {
  console.log('received contact form body', event.body)

  const body = JSON.parse(event.body);
  const sender_name = body.name
  const sender_email = body.email
  const email_body = body.body

  console.log('setup mailjet email')
  // using Twilio SendGrid's v3 Node.js Library
  // https://github.com/sendgrid/sendgrid-nodejs
  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(SENDGRID_API_KEY);
  const msg = {
    to: 'nick@nickmcburney.co.uk',
    from: 'nick@nickmcburney.co.uk',
    templateId: 'd-cdb01f699d9847f6aca5b311d33a2a25',
    dynamic_template_data: {
      name: sender_name,
      email: sender_email,
      message: email_body,
    },
  };
  sgMail.send(msg);
  /*
  const mailjet = require('node-mailjet').connect(
    MailJetAPI,
    MailJetSecret
  )

  const request = mailjet.post("send", {'version': 'v3.1'}).request({
		"Messages":[
			{
				"From": {
					"Email": "nick@nickmcburney.co.uk",
					"Name": "Nick McBurney"
        },
        "ReplyTo": {
          "Email": sender_email,
          "Name": sender_name
        },
				"To": [
					{
						"Email": "nick@nickmcburney.co.uk",
					  "Name": "Nick McBurney"
					}
				],
				"TemplateID": 1355251,
				"TemplateLanguage": true,
				"Subject": "Message from NickMcBurney.co.uk",
				"Variables": {
          "name": sender_name,
          "email": sender_email,
          "body": email_body,
        }
			}
		]
  })
  
  console.log('sending mailjet email')

  request.then((result) => {
      console.log('result')
      console.log(result.body)
    }).catch((err) => {
      console.log('error', err.statusCode)
      console.log(err)
    })

  console.log('end function')
  */
}