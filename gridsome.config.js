module.exports = {
  siteName: 'Nick McBurney | Front-end Developer',
  siteDescription: 'A WordPress starter for Gridsome',

  prefetch: { mask: '^$' }, // example - disable all prefetch

  templates: {
    MDPost: '/blog/:path',
    MDPortfolio: '/portfolio/:path',
    MDLegal: '/:path'
  },

  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        baseDir: './content/blog/',
        path: '**/*.md',
        typeName: 'MDPost',
        remark: {
          plugins: [
            '@gridsome/remark-prismjs'
          ]
        }
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        baseDir: './content/portfolio/',
        path: '**/*.md',
        typeName: 'MDPortfolio'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        baseDir: './content/legal/',
        path: '**/*.md',
        typeName: 'MDLegal'
      }
    }
  ]
}
